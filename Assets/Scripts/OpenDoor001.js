﻿import UnityEngine.UI;

var TextDisplay : GameObject;
var Distance : float = PlayerCasting.DistanceFromTarget;
var Door1 : GameObject;
var Door2 : GameObject;

function Update() {
    Distance = PlayerCasting.DistanceFromTarget;
    if((Input.GetButtonDown("Action")) && (Distance <= 1.5)){
        OpenDoor();
    }
}

function OnMouseOver() {
    if(Distance <= 1.5){
        TextDisplay.GetComponent.<Text>().text = "Open door";
    }
}

function OnMouseExit() {
    TextDisplay.GetComponent.<Text>().text = "";
}
function OpenDoor() {
    Door1.GetComponent("Animator").enabled = true;
    Door2.GetComponent("Animator").enabled = true;
    yield WaitForSeconds(1);
    Door1.GetComponent("Animator").enabled = false;
    Door2.GetComponent("Animator").enabled = false;
    yield WaitForSeconds(10);
    Door1.GetComponent("Animator").enabled = true;
    Door2.GetComponent("Animator").enabled = true;
    yield WaitForSeconds(1);
    Door1.GetComponent("Animator").enabled = false;
    Door2.GetComponent("Animator").enabled = false;
}