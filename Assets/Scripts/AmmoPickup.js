﻿var AmmoSound : AudioSource;

function OnTriggerEnter (col : Collider) {
    AmmoSound.Play();
    if(GlobalAmmo.LoadedAmmo == 0){
        GlobalAmmo.LoadedAmmo += 9;
        GlobalAmmo.CurrentAmmo += 9;
        this.gameObject.SetActive(false);
    }
    else{
        GlobalAmmo.CurrentAmmo += 18;
        this.gameObject.SetActive(false);
    }
}