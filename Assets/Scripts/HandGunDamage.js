﻿var DamageAmount : int = 5;
var TargetDistance : float;
var AllowedRange : float = 15;

function Update(){
    if((Input.GetButtonDown("Fire1")) && (GlobalAmmo.LoadedAmmo > 0)){
        var Shoot : RaycastHit;
        if(Physics.Raycast (transform.position, transform.TransformDirection(Vector3.forward), Shoot)){
            TargedDistance = Shoot.distance;
            if(TargedDistance < AllowedRange){
                Shoot.transform.SendMessage("DeductPoints", DamageAmount);
            }
        }
    }
}